import { lirioConfig } from "lirio/stackbit";

export default { ...lirioConfig(__dirname), useESM: true};